<?php

/**
 * Get a list of pizza names from DB 
 */
function pizza_get_pizza_type() {
	$types = null;
	$result = db_select('pizza_type', 'p')->fields('p')->execute();
	foreach ($result as $row) {
		$pid = $row->pid;

		$name = $row->name;
		$about = $row->about;
		$price = $row->price;
		$available = $row->available;

		$types[$pid]["name"] = $name;
		$types[$pid]["about"] = $about;
		$types[$pid]["price"] = $price;
		$types[$pid]["available"] = $available;
	}
	return $types;
}

/**
 * Get a list of names of delivery areas from DB
 */
function pizza_get_pizza_area() {
	$areas = null;
	$result = db_select('pizza_area', 'p')->fields('p')->execute();
	foreach ($result as $row) {
		$aid = $row->aid;

		$name = $row->name;
		$price = $row->price;
		$available = $row->available;

		$areas[$aid]["name"] = $name;
		$areas[$aid]["price"] = $price;
		$areas[$aid]["available"] = $available;
	}
	return $areas;
}

/**
 * Order Form
 */
function pizza_form($form, &$form_state) {
	$types = pizza_get_pizza_type();
	$areas = pizza_get_pizza_area();

	foreach ($types as $key => $name) {
		if (!$name['available']) {
			unset($types[$key]);
		}
	}

	foreach ($areas as $key => $name) {
		if (!$name['available']) {
			unset($areas[$key]);
		}
	}

	$form['quantity'] = [
		'#type' => 'fieldset',
		'#tree' => TRUE,
	];

	foreach ($types as $key => $name) {
		$form['quantity'][$key] = [
			'#title' => "<a title=\"" . $name["about"] . "\">" . $name["name"] . "</a>" . " Цена за штуку: " . $name["price"] . "₽",
			'#type' => 'select',
			'#options' => range(0, 10),
		];
	}

	foreach ($areas as $key => $name) {
		$areas[$key]['name'] .= '. Цена доставки: ' . $areas[$key]['price'];
	}

	$form['area'] = [
		'#type' => 'radios',
		'#options' => array_combine(array_keys($areas), array_column($areas, 'name')),
		'#required' => TRUE,
		'#title' => 'Ваш район',
	];

	$form['phone'] = [
		'#type' => 'textfield',
		'#title' => 'Телефон',
		'#required' => TRUE,
	];

	$form['address'] = [
		'#type' => 'textfield',
		'#title' => 'Адрес',
		'#required' => TRUE,
	];

	$form['price'] = [
		'#type' => 'textfield',
		'#title' => t('Сумма к оплате'),
		'#disabled' => TRUE,
		'#id' => 'price',
	];

	$form['submit'] = [
		"#type" => "submit",
		'#value' => t('Отправить'),
	];

	drupal_add_js(drupal_get_path('module', 'pizza') . '/js/pizza.js');

	$settings = array('types' => $types, 'areas' => $areas,);
	drupal_add_js(array("pizza" => $settings), 'setting');

	return $form;
}

/**
 * Validation order form
 */
function pizza_form_validate($form, &$form_state) {
	if (isset($form_state['values']['phone'])) {
		$phone = $form_state['values']['phone'];

		if (!preg_match('^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$^', $phone)) {
			form_set_error('phone', t('Phone write ERROR'));
		}
	}
}

/**
 * Submit order form
 */
function pizza_form_submit($form, &$form_state) {
	$types = pizza_get_pizza_type();
	$areas = pizza_get_pizza_area();

	$module = 'pizza';
	$key = 'pizza';
	$to = variable_get('site_mail', '');
	$language = 'ru';

	$count = 0;
	$message_order_pizzas = null;
	foreach ($types as $key => $name) {
		if ($form_state['values']["quantity"][$key] > 0) {
			$count = $form_state['values']["quantity"][$key];
			$message_order_pizzas = $message_order_pizzas . $name["name"] . " в количестве " . $count . " штук за " . $name["price"] . "₽, ";
		}
	}

	$params = array(
		'phone' => "тел. = " . $form_state['values']['phone'],
		'address' => " по адрессу - " . $form_state['values']['address'],
		'quantity' => " " . $message_order_pizzas,
		'block' => $form['area']['#options'][$form_state['values']['area']],
	);

	drupal_mail($module, $key, $to, $language, $params, $from = NULL, $send = TRUE);
	drupal_set_message("Спасибо за заказ! Ожидайте");
	watchdog('forma', '%params', array('%params' => implode(',', $params)));
}

/**
 * Module Settings Form
 */
function pizza_settings($form, &$form_state) {
	drupal_add_css(drupal_get_path('module', 'pizza') . '/css/pizza.css');

	$types = pizza_get_pizza_type();
	$areas = pizza_get_pizza_area();

	$avail = null;
	$index = 1;

	foreach ($types as $key => $name) {
		if ($name['available'] == 0) {
			$avail[$index] = -1;
			$avails[$index] = $name['name'] . "<a class=\"pizzalink\" href=\"pizza-delete/" . $key . "\">delete</a>";
		} else {
			$avail[$index] = $index;
			$avails[$index] = $name['name'] . "<a class=\"pizzalink\" href=\"pizza-delete/" . $key . "\">delete</a>";
		}
		$index = $index + 1;
	}

	$form['pizzas'] = [
		'#type' => 'checkboxes',
		'#title' => "Доступные районы",
		'#default_value' => $avail,
		'#options' => $avails,
	];

	$form['add-pizza'] = array(
		'#type' => 'markup',
		'#markup' => "<a href=\"pizza-add\">+ add new pizza type</a>",
		'#suffix' => '</br></br>',
	);

	$form['pizzas_price'] = [
		'#type' => 'fieldset',
		'#tree' => TRUE,
		'#title' => 'Цены'
	];

	foreach ($types as $key => $name) {
		$form['pizzas_price'][$key] = [
			'#attributes' => [
				'type' => 'number',
			],
			'#type' => 'textfield',
			'#title' => 'Цена ' . $name['name'],
			'#default_value' => $name['price'],
		];
	}

	$avails = null;
	$avail = null;
	$index = 1;

	foreach ($areas as $key => $name) {
		if ($name['available'] == 0) {
			$avail[$index] = -1;
			$avails[$index] = $name['name'] . "<a class=\"pizzalink\" href=\"area-delete/" . $key . "\">delete</a>";
		} else {
			$avail[$index] = $index;
			$avails[$index] = $name['name'] . "<a class=\"pizzalink\" href=\"area-delete/" . $key . "\">delete</a>";
		}
		$index = $index + 1;
	}

	$form['places'] = [
		'#type' => 'checkboxes',
		'#title' => t("Доступные районы"),
		'#default_value' => $avail,
		'#options' => $avails,
	];

	$form['add-area'] = array(
		'#type' => 'markup',
		'#markup' => "<a href=\"area-add\">+ add new area type</a>",
		'#suffix' => '</br></br>',
	);

	$form['places_price'] = [
		'#title' => 'Цена доставки в район',
		'#type' => 'fieldset',
		'#tree' => TRUE
	];

	foreach ($areas as $key => $name) {
		$form['places_price'][$key] = [
			'#attributes' => [
				'type' => 'number',
			],
			'#type' => 'textfield',
			'#title' => $name['name'],
			'#default_value' => $name['price']
		];
	}

	$form['submit'] = [
		"#type" => "submit",
		'#value' => t('Change config pizza order'),
	];
	return $form;
}

/**
 * Validation settings form
 */
function pizza_settings_validate($form, &$form_state) {
    $types = pizza_get_pizza_type();
	$areas = pizza_get_pizza_area();

    foreach ($areas as $key => $name) {
		$price_values = $form_state['values']["places_price"][$key];
        if(!(is_numeric($price_values) && $price_values > 0)){
            form_set_error('price_values', t($name['name'] . " Price not Integer"));
        }
    }

    foreach ($types as $key => $name) {
		$price_values = $form_state['values']["pizzas_price"][$key];
        if(!(is_numeric($price_values) && $price_values > 0)){
            form_set_error('price_values', t($name['name'] . " Price not Integer"));
        }
    }
}

/**
 * Submit Module Settings Form
 */
function pizza_settings_submit($form, &$form_state) {
	$types = pizza_get_pizza_type();
	$areas = pizza_get_pizza_area();
	$checkbox_values = null;

	$index = 1;
	foreach ($areas as $key => $name) {
		$checkbox_values[$index] = $form_state['values']["places"][$index];
		$price_values[$index] = $form_state['values']["places_price"][$key];

		if ($checkbox_values[$index] == 0) {
			db_update('pizza_area')
				->fields(array('available' => 0))
				->condition('aid', $key, '=')
				->execute();
		} else {
			db_update('pizza_area')
				->fields(array('available' => 1))
				->condition('aid', $key, '=')
				->execute();
		}

		db_update('pizza_area')
			->fields(array('price' => $price_values[$index]))
			->condition('aid', $key, '=')
			->execute();

		$index = $index + 1;
	}

	$index = 1;
	foreach ($types as $key => $name) {
		$checkbox_values[$index] = $form_state['values']["pizzas"][$index];
		$price_values[$index] = $form_state['values']["pizzas_price"][$key];

		if ($checkbox_values[$index] == 0) {
			db_update('pizza_type')
				->fields(array('available' => 0))
				->condition('pid', $key, '=')
				->execute();
		} else {
			db_update('pizza_type')
				->fields(array('available' => 1))
				->condition('pid', $key, '=')
				->execute();
		}

		db_update('pizza_type')
			->fields(array('price' => $price_values[$index]))
			->condition('pid', $key, '=')
			->execute();

		$index = $index + 1;
	}

	drupal_set_message("Изменения сохранены");
}

/**
 * Pizza Add Form
 */
function pizza_add_pizza($form, &$form_state) {
	$form['pizza-name'] = array(
		'#title' => t('Name pizza'),
		'#type' => 'textfield',
	);
	$form['pizza-about'] = array(
		'#title' => t('Description'),
		'#type' => 'textarea',
		'#required' => TRUE,
	);
	$form['pizza-price'] = [
		'#attributes' => [
			'type' => 'number',
		],
		'#type' => 'textfield',
		'#title' => "Price",
		'#default_value' => 0
	];
	$form['submit'] = [
		"#type" => "submit",
		'#value' => t('Создать'),
	];
	return $form;
}

/**
 * Validation pizza_add form
 */
function pizza_add_pizza_validate($form, &$form_state) {
	$price_values = $form_state['values']["pizza-price"];
    if(!(is_numeric($price_values) && $price_values > 0)){
        form_set_error('price_values', t("Price not Integer"));
    }
}

/**
 * Submit Add Form
 */
function pizza_add_pizza_submit($form, &$form_state) {
	$name_values = $form_state['values']["pizza-name"];
	$about_values = $form_state['values']["pizza-about"];
	$price_values = $form_state['values']["pizza-price"];

	db_insert('pizza_type')
		->fields(
			array(
				'name' => $name_values,
				'about' => $about_values,
				'price' => $price_values,
				'available' => '1',
			)
		)
		->execute();

	drupal_goto("pizza/settings");
	drupal_set_message("Добавленно");
}

/**
 * Remove pizza 
 */
function pizza_delete_pizza($nid) {
	db_delete('pizza_type')
		->condition('pid', $nid)
		->execute();
	drupal_goto("pizza/settings");
	drupal_set_message("Удаленно");
}

/**
 * Form add a area
 */
function pizza_add_area($form, &$form_state) {
    $form['area-name'] = array(
		'#title' => t('Name pizza'),
		'#type' => 'textfield',
	);
	$form['area-price'] = [
		'#attributes' => [
			'type' => 'number',
		],
		'#type' => 'textfield',
		'#title' => "Price",
		'#default_value' => 0
	];
	$form['submit'] = [
		"#type" => "submit",
		'#value' => t('Создать'),
	];
	return $form;
}

/**
 * Validation area_add form
 */
function pizza_add_area_validate($form, &$form_state)
{
	$price_values = $form_state['values']["area-price"];
    if(!(is_numeric($price_values) && $price_values > 0)){
        form_set_error('price_values', t("Price not Integer"));
    }
}

/**
 * Submit form add a area
 */
function pizza_add_area_submit($form, &$form_state) {
	$name_values = $form_state['values']["area-name"];
	$price_values = $form_state['values']["area-price"];

	db_insert('pizza_area')
		->fields(
			array(
				'name' => $name_values,
				'price' => $price_values,
				'available' => '1'
			)
		)
		->execute();

	drupal_goto("pizza/settings");
	drupal_set_message("Добавленно");
}

/**
 * Remove area
 */
function pizza_delete_area($nid) {
	db_delete('pizza_area')
		->condition('aid', $nid)
		->execute();
	drupal_goto("pizza/settings");
	drupal_set_message("Удаленно");
}

?>
